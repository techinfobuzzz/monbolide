import React from 'react';
import $ from 'jquery';
import Locale from '../../helpers/locale.json';

class FaqList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <section className="content">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
              </div>
              <div className="card-body">
                <table id="example1" className="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th style={{width: "20%"}}>Faq question</th>
                    <th>Faq answer</th>
                    <th style={{width: "10%"}}>{Locale.status}</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>Why Monbolide </td>
                    <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                    <td>
                        <span style={{padding: "6px"}} class="badge badge-success">Active</span>
                    </td>
                    <td style={{width: "25%"}}>
                        <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-folder">
                            </i>
                            View
                        </a>
                        <a className="btn btn-info btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-pencil-alt">
                            </i>
                            Edit
                        </a>
                        <a className="btn btn-danger btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-trash">
                            </i>
                            Delete
                        </a>
                    </td>
                  </tr>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
                </div>
              </div>
            </div>
          </div>
        </section>
    )
  }
}

export default FaqList;

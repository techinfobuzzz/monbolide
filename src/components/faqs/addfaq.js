import React from 'react';
import $ from 'jquery';
import swal from 'sweetalert';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux'
import Locale from '../../helpers/locale.json';
import { EmailValidation , PhoneNumberValidation } from '../../common/validation.js';
import { AC_ADD_EMPLOYEE_PEROSNAL_DATA } from '../actions/employee';

class AddFaq extends React.Component {
 constructor(props) {
    super(props);
    this.state = {
      question        : null,
      answer          : null,
      questionError   : 'Question is Required',
      answerError     : 'Answer number is Required'
    };
    this.handleSubmit       = this.handleSubmit.bind(this);
    this.handleInputChange  = this.handleInputChange.bind(this);
  }

  handleSubmit=(e)=> {
    e.preventDefault();
    var data = this.validateForm();
    this.props.AddEmployeePersonalInfo(data);
    // if(data) {
    //   this.props.AddEmployeePersonalInfo(data);
    // } else {
    //   swal({
    //     title: "Please check the form",
    //     text: "Please fill required fields",
    //     icon: "error",
    //     dangerMode: true
    //     })
    // }
  }

  validateForm() {
    let question        = this.state.question;
    let answer          = this.state.answer;
      if(question && answer) {
          var personalInfoData = {};
          personalInfoData.question = question;
          personalInfoData.answer   = answer;
         return personalInfoData;
      } else {
          return false;
      }
  }

  handleInputChange=(event)=> {

  }

  render() {
    return(
      <section className="content">
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">{Locale.addfaq}</h3>
                  <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i className="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div className="card-body">
                  <div className="form-group">
                    <label>{Locale.question}</label><span className="error-class">*</span>
                    <input placeholder={Locale.question} type="text" name="question" className="form-control" onChange={this.handleInputChange}/>
                    { !this.state.question ? <span className="error-class">{Locale.question} is Required</span> : "" }
                  </div>
                  <div className="form-group">
                    <label>{Locale.answer}</label><span className="error-class">*</span>
                    <textarea placeholder={Locale.answer} name="answer" className="form-control" rows="4" onChange={this.handleInputChange}></textarea>
                    { !this.state.answer ? <span className="error-class">{Locale.answer} is Required</span> : "" }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <input type="submit" value={Locale.save} className="btn btn-success float-left"/>
            </div>
          </div>
     </form>
   </section>
  )
 }
}

function mapStateToProps(state) {
  return { state };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({AddEmployeePersonalInfo:AC_ADD_EMPLOYEE_PEROSNAL_DATA }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddFaq)

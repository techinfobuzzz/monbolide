import React from 'react';
import $ from 'jquery';
import swal from 'sweetalert';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux'
import Locale from '../../helpers/locale.json';
import { EmailValidation , PhoneNumberValidation } from '../../common/validation.js';
import { AC_ADD_EMPLOYEE_PEROSNAL_DATA } from '../actions/employee';

class AddFaq extends React.Component {
 constructor(props) {
    super(props);
    this.state = {
      languagename          : null,
      languagecode          : null,
      languagenameError     : 'languagename is Required',
      languagecodeError     : 'languagecode number is Required'
    };
    this.handleSubmit       = this.handleSubmit.bind(this);
    this.handleInputChange  = this.handleInputChange.bind(this);
  }

  handleSubmit=(e)=> {
    e.preventDefault();
    var data = this.validateForm();
    this.props.AddEmployeePersonalInfo(data);
    // if(data) {
    //   this.props.AddEmployeePersonalInfo(data);
    // } else {
    //   swal({
    //     title: "Please check the form",
    //     text: "Please fill required fields",
    //     icon: "error",
    //     dangerMode: true
    //     })
    // }
  }

  validateForm() {
    let languagename          = this.state.languagename;
    let languagecode          = this.state.languagecode;
      if(languagename && languagecode) {
          var personalInfoData = {};
          personalInfoData.languagename = languagename;
          personalInfoData.languagecode   = languagecode;
         return personalInfoData;
      } else {
          return false;
      }
  }

  handleInputChange=(event)=> {

  }

  render() {
    return(
      <section className="content">
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">{Locale.addcountry}</h3>
                  <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i className="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div className="card-body">
                  <div className="form-group">
                    <label>{Locale.languagename}</label><span className="error-class">*</span>
                    <input placeholder={Locale.languagename} type="text" name="languagename" className="form-control" onChange={this.handleInputChange}/>
                    { !this.state.languagename ? <span className="error-class">{Locale.languagename} is Required</span> : "" }
                  </div>
                  <div className="form-group">
                    <label>{Locale.languagecode}</label><span className="error-class">*</span>
                    <input type="text" placeholder={Locale.languagecode} name="languagecode" className="form-control"  onChange={this.handleInputChange}/>
                    { !this.state.languagecode ? <span className="error-class">{Locale.languagecode} is Required</span> : "" }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <input type="submit" value={Locale.save} className="btn btn-success float-left"/>
            </div>
          </div>
     </form>
   </section>
  )
 }
}

function mapStateToProps(state) {
  return { state };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({AddEmployeePersonalInfo:AC_ADD_EMPLOYEE_PEROSNAL_DATA }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddFaq)

import React from 'react';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router' ;
import { ConnectedRouter } from 'connected-react-router';

// Common management
import Header from './components/layouts/header';
import Sidebar from './components/layouts/sidebar';
import Breadcrumb from './components/layouts/breadcrumb';

// Dashboard management
import Dashboard from './components/dashboard';

// AdminSettings management
import AdminSettings from './components/settings/adminsettings';

// Faq management
import FaqList from './components/faqs/faqlist';
import AddFaq from './components/faqs/addfaq';

// Language management
import LanguageList from './components/languages/languagelist';
import AddLanguage from './components/languages/addlanguage';

// Language management
import CountryList from './components/country/countrylist';
import AddCountry from './components/country/addcountry';

// Pages management
import PagesList from './components/pages/pageslist';
import AddPage from './components/pages/addpage';

// Pages management
import TemplatesList from './components/templates/templateslist';
import AddTemplate from './components/templates/addtemplate';

// Subscribers management
import SubscribersList from './components/subscribers/subscriberslist';

// middleware configuration
import configureStore, { history } from './configureStore';
const store = configureStore();

function App() {
  return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
           <Header/>
           <Sidebar/>
            <Switch>
               <div className="content-wrapper">
                 <section className="content">
                 <Breadcrumb />
                     <div className="container-fluid">
                        <Route path="/dashboard" component={Dashboard} />
                        <Route path="/adminSettings" component={AdminSettings} />
                        <Route path="/listFaq" component={FaqList} />
                        <Route path="/addFaq" component={AddFaq} />
                        <Route path="/subscribers" component={SubscribersList} />
                        <Route path="/listLanguage" component={LanguageList} />
                        <Route path="/addLanguage" component={AddLanguage} />
                        <Route path="/listCountry" component={CountryList} />
                        <Route path="/addCountry" component={AddCountry} />
                        <Route path="/listPages" component={PagesList} />
                        <Route path="/addPage" component={AddPage} />
                        <Route path="/listTemplates" component={TemplatesList} />
                        <Route path="/addTemplate" component={AddTemplate} />
                     </div>
                  </section>
               </div>
            </Switch>
        </ConnectedRouter>
      </Provider>
  );
}

export default App;

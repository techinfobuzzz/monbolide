import React from 'react';
import $ from 'jquery';

class SubscribersList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <section className="content">
        <div className="card">
          <div className="card-header">
            <h3 className="card-title">Subscribers</h3>

            <div className="card-tools">
              <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                <i className="fas fa-minus"></i></button>
              <button type="button" className="btn btn-tool" data-card-widget="remove" data-toggle="tooltip" title="Remove">
                <i className="fas fa-times"></i></button>
            </div>
          </div>
          <div className="card-body p-0">
            <table className="table table-striped projects">
                <thead>
                    <tr>
                        <th style={{width: "15%"}}>
                            #
                        </th>
                        <th style={{width: "35%"}}>
                            Email
                        </th>
                        <th style={{width: "25%"}}>
                            Date
                        </th>

                        <th  style={{width: "25%"}}>
                            Action
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style={{width: "15%"}}>
                            1
                        </td>
                        <td style={{width: "35%"}}>
                            <a>
                                Andrew@gmail.com
                            </a>
                        </td>
                        <td style={{width: "25%"}}>
                             01.01.2020
                        </td>
                        <td style={{width: "25%"}}>
                            <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                                <i className="fas fa-folder" style={{margin:"4px"}}></i>
                                Send Mail
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width: "15%"}}>
                            2
                        </td>
                        <td style={{width: "35%"}}>
                            <a>
                                blake@gmail.com
                            </a>
                        </td>
                        <td style={{width: "25%"}}>
                             21.01.2020
                        </td>
                        <td style={{width: "25%"}}>
                            <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                                <i className="fas fa-folder" style={{margin:"4px"}}></i>
                                Send Mail
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width: "15%"}}>
                            3
                        </td>
                        <td style={{width: "35%"}}>
                            <a>
                                symonds@gmail.com
                            </a>
                        </td>
                        <td style={{width: "25%"}}>
                             05.10.2020
                        </td>
                        <td style={{width: "25%"}}>
                            <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                                <i className="fas fa-folder" style={{margin:"4px"}}></i>
                                Send Mail
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td style={{width: "15%"}}>
                            4
                        </td>
                        <td style={{width: "35%"}}>
                            <a>
                                vincent@gmail.com
                            </a>
                        </td>
                        <td style={{width: "25%"}}>
                             04.05.2020
                        </td>
                        <td style={{width: "25%"}}>
                            <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                                <i className="fas fa-folder" style={{margin:"4px"}}></i>
                                Send Mail
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
          </div>
        </div>
      </section>
    )
  }
}

export default SubscribersList;

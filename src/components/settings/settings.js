import React from 'react';
import $ from 'jquery';
import Locale from '../../helpers/locale.json';

class Social extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange  = this.handleInputChange.bind(this);
    this.saveSocialSettings  = this.saveSocialSettings.bind(this);
    this.fileToImageConversation  = this.fileToImageConversation.bind(this);
  }

  saveSocialSettings() {
    console.log('save');
  }

  handleInputChange(event) {
    this.fileToImageConversation(event)
  }

  fileToImageConversation(event) {
     var reader = new FileReader();
     reader.onload = function()  {
      var output = document.getElementById('siteavicon');
      output.src = reader.result;
     }
     reader.readAsDataURL(event.target.files[0]);
  }

  componentDidMount() {
    window.$('#example1').DataTable();
  }

  render() {
    return(
      <div className="tab-pane fade show active" id="custom-content-below-home" role="tabpanel" aria-labelledby="custom-content-below-home-tab">
        <div className="card-body">
            <div className="row">
              <div className="col-md-6">
                <div className="form-group">
                  <label>{Locale.contactemail}</label>
                  <input placeholder={Locale.contactemail} type="text" name="contactemail" className="form-control"/>
                </div>
                <div className="form-group">
                  <label>{Locale.sitename}</label>
                  <input placeholder={Locale.sitename} type="text" name="sitename" className="form-control"/>
                </div>
                <div className="form-group">
                  <label>{Locale.contactnumber}</label>
                  <input placeholder={Locale.contactnumber} type="text" name="contactnumber" className="form-control"/>
                </div>
                <div className="form-group">
                  <label>{Locale.footercontent}</label>
                  <input placeholder={Locale.footercontent} type="text" name="footercontent" className="form-control"/>
                </div>
                <div className="form-group">
                  <label>{Locale.sitelogo}</label><span className="error-class">*</span>
                  <div>
                      <input type="file"  name="sitelogo" id="sitelogo" onChange={this.handleInputChange}/>
                  </div>
                </div>
                <div className="form-group">
                  <div className="sitelogo"></div>
               </div>
               <div className="form-group">
                 <label>{Locale.siteavicon}</label><span className="error-class">*</span>
                 <div>
                     <input type="file"  name="siteavicon" id="siteavicon" onChange={this.handleInputChange}/>
                 </div>
               </div>
               <div className="form-group">
                 <div id="siteavicon" className="siteavicon"></div>
              </div>
              <div className="form-group">
                <input type="submit" value={Locale.save} className="btn btn-success float-right" onClick={this.saveSocialSettings}/>
              </div>
            </div>
        </div>
       </div>
     </div>
    )
  }
}

export default Social;

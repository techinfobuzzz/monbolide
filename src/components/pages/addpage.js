import React from 'react';
import $ from 'jquery';
import swal from 'sweetalert';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux'
import Locale from '../../helpers/locale.json';
import { EmailValidation , PhoneNumberValidation } from '../../common/validation.js';
import { AC_ADD_EMPLOYEE_PEROSNAL_DATA } from '../actions/employee';

class AddPage extends React.Component {
 constructor(props) {
    super(props);
    this.state = {
      pagename          : null,
      pagedescription          : null,
      pagenameError     : 'pagename is Required',
      pagedescriptionError     : 'pagedescription number is Required'
    };
    this.handleSubmit       = this.handleSubmit.bind(this);
    this.handleInputChange  = this.handleInputChange.bind(this);
  }

  componentDidMount() {
    window.$(function () {
      window.$('#compose-textarea').summernote()
    })
  }

  handleSubmit=(e)=> {
    e.preventDefault();
    var data = this.validateForm();
    this.props.AddEmployeePersonalInfo(data);
    // if(data) {
    //   this.props.AddEmployeePersonalInfo(data);
    // } else {
    //   swal({
    //     title: "Please check the form",
    //     text: "Please fill required fields",
    //     icon: "error",
    //     dangerMode: true
    //     })
    // }
  }

  validateForm() {
    let pagename          = this.state.pagename;
    let pagedescription          = this.state.pagedescription;
      if(pagename && pagedescription) {
          var personalInfoData = {};
          personalInfoData.pagename = pagename;
          personalInfoData.pagedescription   = pagedescription;
         return personalInfoData;
      } else {
          return false;
      }
  }

  handleInputChange=(event)=> {

  }

  render() {
    return(
      <section className="content">
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">{Locale.addpage}</h3>
                  <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i className="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div className="card-body">
                  <div className="form-group">
                    <label>{Locale.pagename}</label><span className="error-class">*</span>
                    <input placeholder={Locale.pagename} type="text" name="pagename" className="form-control" onChange={this.handleInputChange}/>
                    { !this.state.pagename ? <span className="error-class">{Locale.pagename} is Required</span> : "" }
                  </div>
                  <div className="form-group">
                    <label>{Locale.subject}</label><span className="error-class">*</span>
                    <input placeholder={Locale.subject} type="text" name="subject" className="form-control" onChange={this.handleInputChange}/>
                    { !this.state.subject ? <span className="error-class">{Locale.subject} is Required</span> : "" }
                  </div>
                  <div class="form-group">
                      <label>{Locale.pagedescription}</label><span className="error-class">*</span>
                      <textarea id="compose-textarea"   style={{height:'500px'}} >  </textarea>
                      { !this.state.pagedescription ? <span className="error-class">{Locale.pagedescription} is Required</span> : "" }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <input type="submit" value={Locale.save} className="btn btn-success float-left"/>
            </div>
          </div>
     </form>
   </section>
  )
 }
}

function mapStateToProps(state) {
  return { state };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({AddEmployeePersonalInfo:AC_ADD_EMPLOYEE_PEROSNAL_DATA }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddPage)

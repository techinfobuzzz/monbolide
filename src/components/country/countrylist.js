import React from 'react';
import $ from 'jquery';
import Locale from '../../helpers/locale.json';

class CountryList extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return(
      <section className="content">
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-header">
              </div>
              <div className="card-body">
                <table id="example1" className="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>#</th>
                    <th>{Locale.countryname}</th>
                    <th>{Locale.countrycode}</th>
                    <th>{Locale.status}</th>
                    <th>Action</th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td>1</td>
                    <td>India</td>
                    <td>In</td>
                    <td>
                        <span style={{padding: "6px"}} class="badge badge-success">Active</span>
                    </td>
                    <td style={{width: "25%"}}>
                        <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-folder" style={{margin:"4px"}}>
                            </i>
                            View
                        </a>
                        <a className="btn btn-info btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-pencil-alt" style={{margin:"4px"}}>
                            </i>
                            Edit
                        </a>
                        <a className="btn btn-danger btn-sm" href="#" style={{marginRight: "10px"}}>
                            <i className="fas fa-trash" style={{margin:"4px"}}>
                            </i>
                            Delete
                        </a>
                     </td>
                   </tr>
                   <tr>
                     <td>2</td>
                     <td>France</td>
                     <td>Fr</td>
                     <td>
                         <span style={{padding: "6px"}} class="badge badge-danger">Deactive</span>
                     </td>
                     <td style={{width: "25%"}}>
                         <a className="btn btn-primary btn-sm" href="#" style={{marginRight: "10px"}}>
                             <i className="fas fa-folder" style={{margin:"4px"}}>
                             </i>
                             View
                         </a>
                         <a className="btn btn-info btn-sm" href="#" style={{marginRight: "10px"}}>
                             <i className="fas fa-pencil-alt" style={{margin:"4px"}}>
                             </i>
                             Edit
                         </a>
                         <a className="btn btn-danger btn-sm" href="#" style={{marginRight: "10px"}}>
                             <i className="fas fa-trash" style={{margin:"4px"}}>
                             </i>
                             Delete
                         </a>
                      </td>
                    </tr>
                  </tbody>
                  <tfoot>
                  </tfoot>
                </table>
                </div>
              </div>
            </div>
          </div>
        </section>
    )
  }
}

export default CountryList;

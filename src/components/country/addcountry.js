import React from 'react';
import $ from 'jquery';
import swal from 'sweetalert';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux'
import Locale from '../../helpers/locale.json';
import { EmailValidation , PhoneNumberValidation } from '../../common/validation.js';
import { AC_ADD_EMPLOYEE_PEROSNAL_DATA } from '../actions/employee';

class AddCountry extends React.Component {
 constructor(props) {
    super(props);
    this.state = {
      countryname          : null,
      countrycode          : null,
      countrynameError     : 'countryname is Required',
      countrycodeError     : 'countrycode number is Required'
    };
    this.handleSubmit       = this.handleSubmit.bind(this);
    this.handleInputChange  = this.handleInputChange.bind(this);
  }

  handleSubmit=(e)=> {
    e.preventDefault();
    var data = this.validateForm();
    this.props.AddEmployeePersonalInfo(data);
    // if(data) {
    //   this.props.AddEmployeePersonalInfo(data);
    // } else {
    //   swal({
    //     title: "Please check the form",
    //     text: "Please fill required fields",
    //     icon: "error",
    //     dangerMode: true
    //     })
    // }
  }

  validateForm() {
    let countryname          = this.state.countryname;
    let countrycode          = this.state.countrycode;
      if(countryname && countrycode) {
          var personalInfoData = {};
          personalInfoData.countryname = countryname;
          personalInfoData.countrycode   = countrycode;
         return personalInfoData;
      } else {
          return false;
      }
  }

  handleInputChange=(event)=> {

  }

  render() {
    return(
      <section className="content">
        <form onSubmit={this.handleSubmit}>
          <div className="row">
            <div className="col-md-6">
              <div className="card card-primary">
                <div className="card-header">
                  <h3 className="card-title">{Locale.addlanguage}</h3>
                  <div className="card-tools">
                    <button type="button" className="btn btn-tool" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                      <i className="fas fa-minus"></i>
                    </button>
                  </div>
                </div>
                <div className="card-body">
                  <div className="form-group">
                    <label>{Locale.countryname}</label><span className="error-class">*</span>
                    <input placeholder={Locale.countryname} type="text" name="countryname" className="form-control" onChange={this.handleInputChange}/>
                    { !this.state.countryname ? <span className="error-class">{Locale.countryname} is Required</span> : "" }
                  </div>
                  <div className="form-group">
                    <label>{Locale.countrycode}</label><span className="error-class">*</span>
                    <input type="text" placeholder={Locale.countrycode} name="countrycode" className="form-control"  onChange={this.handleInputChange}/>
                    { !this.state.countrycode ? <span className="error-class">{Locale.countrycode} is Required</span> : "" }
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12">
              <input type="submit" value={Locale.save} className="btn btn-success float-left"/>
            </div>
          </div>
     </form>
   </section>
  )
 }
}

function mapStateToProps(state) {
  return { state };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({AddEmployeePersonalInfo:AC_ADD_EMPLOYEE_PEROSNAL_DATA }, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(AddCountry)

import React from 'react';
import $ from 'jquery';
import swal from 'sweetalert';
import { connect, } from 'react-redux';
import { bindActionCreators } from 'redux';
import Locale from '../../helpers/locale.json';
import { EmailValidation , PhoneNumberValidation } from '../../common/validation.js';
import { AC_ADD_EMPLOYEE_PEROSNAL_DATA } from '../actions/employee';
import Social from './social';
import Smtp from './smtp';
import Settings from './settings';

class EmployeeList extends React.Component {
  constructor(props) {
    super(props);
    this.handleInputChange  = this.handleInputChange.bind(this);
    this.saveSocialSettings  = this.saveSocialSettings.bind(this);
  }

  handleInputChange(event) {
    this.fileToImageConversation(event)
  }

  saveSocialSettings() {
    console.log('-===============');
  }

  render() {
    return(
      <section className="content">
       <div className="card card-primary card-outline">
        <div className="card-body">
          <ul className="nav nav-tabs" id="custom-content-below-tab" role="tablist">
            <li className="nav-item">
              <a className="nav-link active" id="custom-content-below-home-tab" data-toggle="pill" href="#custom-content-below-home" role="tab" aria-controls="custom-content-below-home" aria-selected="true">Site Settings</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="custom-content-below-profile-tab" data-toggle="pill" href="#custom-content-below-profile" role="tab" aria-controls="custom-content-below-profile" aria-selected="false">Social Media</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="custom-content-below-messages-tab" data-toggle="pill" href="#custom-content-below-messages" role="tab" aria-controls="custom-content-below-messages" aria-selected="false">Google Webmaster & SEO</a>
            </li>
            <li className="nav-item">
              <a className="nav-link" id="custom-content-below-settings-tab" data-toggle="pill" href="#custom-content-below-settings" role="tab" aria-controls="custom-content-below-settings" aria-selected="false">App Settings</a>
            </li>
          </ul>
            <div className="tab-content" id="custom-content-below-tabContent">
                <Settings/>
              <div className="tab-pane fade" id="custom-content-below-profile" role="tabpanel" aria-labelledby="custom-content-below-profile-tab">
                <Social/>
              </div>
              <div className="tab-pane fade" id="custom-content-below-messages" role="tabpanel" aria-labelledby="custom-content-below-messages-tab">
                <Smtp/>
              </div>
              <div className="tab-pane fade" id="custom-content-below-settings" role="tabpanel" aria-labelledby="custom-content-below-settings-tab">
                <Smtp/>
              </div>
           </div>
       </div>
     </div>
   </section>
  )
 }
}

export default EmployeeList;
